package org.gradle;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class PrinterTest {
	
	private static Logger logger = Logger.getLogger(PrinterTest.class);
	@BeforeClass
	public static void beforeClass() {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ALL);
	}

/*	@Test
	public void testPrint() throws IOException {
		Printer printer = new Printer(new Component(new File("src/test/resources/test/data.json")));
		printer.pr
		System.out.println(printer.print());
	}*/
	
	@Test
	public void testPrintObject_Widget() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expWidget.txt");
		final String act = printer.printObject(holder.getComponents()[0]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintObject_Table() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expTable.txt");
		final String act = printer.printObject(holder.getComponents()[1]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintStep_Table() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expStepTable.txt");
		final String act = printer.printStep(holder.getComponents()[1]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintObject_Table2() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expTable2.txt");
		final String act = printer.printObject(holder.getComponents()[2]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintStep_Table2() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expStepTable2.txt");
		final String act = printer.printStep(holder.getComponents()[2]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintStep_Widget() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expStepWidget.txt");
		final String act = printer.printStep(holder.getComponents()[0]);
		execute(exp, act);
	}
	
	
	@Test
	public void testPrintObject_Overlay() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expOverlay.txt");
		final String act = printer.printObject(holder.getComponents()[3]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintStep_Overlay() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expStepOverlay.txt");
		final String act = printer.printStep(holder.getComponents()[3]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintObject_InnerTable() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expInnerTable.txt");
		final String act = printer.printObject(holder.getComponents()[5]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintStep_InnerTable() throws IOException {
		Component holder = new Component(new File("src/test/resources/test/data.json"));
		Printer printer = new Printer(new Component[] {holder});
		final File exp = new File("src/test/resources/expStepInnerTable.txt");
		final String act = printer.printStep(holder.getComponents()[5]);
		execute(exp, act);
	}
	
	@Test
	public void testPrintObject_PatientMerge() throws IOException {
		File folder = new File("src/test/resources/json");
		Collection<File> files = FileUtils.listFiles(folder, new String[]{"json"}, true);
		
		List<Component> components = new LinkedList<Component>();
		int i = 0;
		for (File file : files) {
/*			if (! file.getName().contains("widget_pharmacyMessage")) {
				continue;
			}*/
			logger.info(String.format("run file[%s]", file.getName()));
			try {
				components.add(new Component(file));
				if (file.getParentFile().getName().equals(folder.getName())) {
					components.get(i).setParentPathName(".");
				} else {
					components.get(i).setParentPathName(file.getParentFile().getName());
				}
				
			} catch (Exception e) {
				throw new IllegalStateException("issue encountered when dealing file[" + file.getName() + "]", e);
			}
			i++;
		}
		Printer printer = new Printer(components.toArray(new Component[]{}));
		String content = printer.print();
		//System.out.println(content);
	}

	private void execute(final File exp, final String act) throws IOException {
		boolean isSave = false;
		if (! isSave) {
			Assert.assertEquals(FileUtils.readFileToString(exp), act);
		} else {
			FileUtils.writeStringToFile(exp, act);
		}
	}
}


