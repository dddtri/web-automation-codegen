package org.gradle;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class ComponentTest {
    @Test
    public void testParse() throws IOException {
        File file = new File("src/test/resources/json/data.json");
		Component holder = new Component(file);
		Component[] subComponents = holder.getComponents();
		Assert.assertEquals(5, subComponents.length);
		
		for (Component subComponent : subComponents) {
			Assert.assertEquals(false, subComponent.getName().isEmpty());
			Assert.assertEquals(false, subComponent.getType().isEmpty());
			Assert.assertEquals(false, subComponent.getTo().isEmpty());
			Assert.assertEquals(true, subComponent.getParent() != null);
			Assert.assertEquals(true, subComponent.getToComponent() != null);
			subComponent.getLocator();
		}
		
    }
}

