package com.drfirst.qa.rcopia4x.web.cucumber.step;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import com.drfirst.qa.drassist.core.exceptions.BrowserException;
import com.drfirst.qa.drassist.cucumber.steps.WebSteps;
import com.drfirst.qa.rcopia4x.data.ExpressionHandler;
//import com.drfirst.qa.rcopia4x.ui.widget.pharmacy.FindApharmacySection;
import com.drfirst.qa.rcopia4x.utilities.MessageFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Supplier;
import com.google.common.base.Predicate;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;

import org.apache.log4j.Logger;
import org.openqa.selenium.TimeoutException;

public class TablePharmacyMessagesSearchResultStep extends WebSteps{
	private static Logger logger = Logger.getLogger(TablePharmacyMessagesSearchResultStep.class);
	
	
	/**
	 * can/cannot see 'x' icon in following rows in 'yyyy' table
	 */
	@And("^I ?(|can|cannot) see 'go to patient data' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void see_go_to_patient_data_icon_in_following_row_in_message_table(List<WebElement> outterRows, String canOrCannot, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		for (WebElement matchRow : matcheRows) {
			if ("can".equals(canOrCannot.toLowerCase()) || canOrCannot.isEmpty()) {
				if (!table.seeGoToPatientDataIcon(matchRow)) {
					throw new BrowserException("'go to patient data' icon is not visible when it should be");
				}
			} else if ("cannot".equals(canOrCannot.toLowerCase())) {
				if (table.seeGoToPatientDataIcon(matchRow)) {
					throw new BrowserException("'go to patient data' icon is visible when it should not be");
				}
			}
		}
		
	}
	
	/**
	 * click 'x' icon in following rows in 'yyyy' table
	 */
	@And("^I click 'go to patient data' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void click_go_to_patient_data_icon_in_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		
		table.clickGoToPatientDataIcon(matcheRows.get(0));
	}
	
	/**
	 * if visible, click 'x' icon in following rows in 'yyyy' table
	 */
	@And("^if visible, I click 'go to patient data' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void if_visible_click_go_to_patient_data_icon_in_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
		List<WebElement> matcheRows = null;
		try {
			matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		} catch (BrowserException e) {
			//occur when no row can be found
			return;
		} catch (TimeoutException e) {
			//occur when table object doesn't exist
			return;
		}
		if (matcheRows.size() <= 0) {
			return;
		}
		
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		if (table.seeGoToPatientDataIcon(matcheRows.get(0))) {
			
			table.clickGoToPatientDataIcon(matcheRows.get(0));
			
		}
		
	}
	
	/**
	 * can/cannot see 'x' icon in following rows in 'yyyy' table
	 */
	@And("^I ?(|can|cannot) see 'patient clinical report' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void see_patient_clinical_report_icon_in_following_row_in_message_table(List<WebElement> outterRows, String canOrCannot, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		for (WebElement matchRow : matcheRows) {
			if ("can".equals(canOrCannot.toLowerCase()) || canOrCannot.isEmpty()) {
				if (!table.seePatientClinicalReportIcon(matchRow)) {
					throw new BrowserException("'patient clinical report' icon is not visible when it should be");
				}
			} else if ("cannot".equals(canOrCannot.toLowerCase())) {
				if (table.seePatientClinicalReportIcon(matchRow)) {
					throw new BrowserException("'patient clinical report' icon is visible when it should not be");
				}
			}
		}
		
	}
	
	/**
	 * click 'x' icon in following rows in 'yyyy' table
	 */
	@And("^I click 'patient clinical report' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void click_patient_clinical_report_icon_in_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		
		table.clickPatientClinicalReportIcon(matcheRows.get(0));
	}
	
	/**
	 * if visible, click 'x' icon in following rows in 'yyyy' table
	 */
	@And("^if visible, I click 'patient clinical report' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void if_visible_click_patient_clinical_report_icon_in_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
		List<WebElement> matcheRows = null;
		try {
			matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		} catch (BrowserException e) {
			//occur when no row can be found
			return;
		} catch (TimeoutException e) {
			//occur when table object doesn't exist
			return;
		}
		if (matcheRows.size() <= 0) {
			return;
		}
		
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		if (table.seePatientClinicalReportIcon(matcheRows.get(0))) {
			
			table.clickPatientClinicalReportIcon(matcheRows.get(0));
			
		}
		
	}

	@And("^I ?(|can|cannot) see 'action' dropdown in following rows? in '?message'? table in patients to keep table$")
	public void see_action_dropdown_in_following_row_in_message_table(List<WebElement> outterRows, String canOrCannot, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		for (WebElement matchRow : matcheRows) {
			if ("can".equals(canOrCannot.toLowerCase()) || canOrCannot.isEmpty()) {
				if (!table.seeActionDropdown(matchRow)) {
					throw new BrowserException("'action' dropdown is not visible when it should be");
				}
			} else if ("cannot".equals(canOrCannot.toLowerCase())) {
				if (table.seeActionDropdown(matchRow)) {
					throw new BrowserException("'action' dropdown is visible when it should not be");
				}
			}
		}
	}
	
	/**
	 * Select 'y' in 'x' dropdown in following rows in 'yyyy' table (from outter table)
	 */
	@And("^I select \"?(.*?)\"? from 'action' dropdown in following rows? in '?message'? table in patients to keep table$")
	public void select_from_action_dropdown_in_following_row_in_message_table(List<WebElement> outterRows, String text, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		for (WebElement matchRow : matcheRows) {
			PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
			table.selectActionDropdown(matchRow, text);
		}
	}
	
	/**
	 * See 'y' value is selected in 'x' dropdown in following rows in 'yyyy' table (from outter table)
	 */
	@And("^I see \"?(.*?)\"? is selected in 'action' dropdown in following rows? in '?message'? table in patients to keep table$")
	public void  see_is_selected_in_action_dropdown_in_following_row_in_message_table(List<WebElement> outterRows, String option, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		for (WebElement matchRow : matcheRows) {
			PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
			table.isActionDropdownSelected(matchRow, option);
		}
	
	}
		
	/**
	 * See 'y' options in 'x' dropdown in following rows in 'yyyy' table (from outter table)
	 */
	@And("^I ?(|can|cannot) see \"?(.*?)\"? options? in 'action' dropdown in following rows? in '?message'? table in patients to keep table$$")
	public void see_options_in_action_dropdown_in_following_row_in_message_table(List<WebElement> outterRows, String canOrCannot, String options, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		int i = 0;
		for (WebElement matchRow : matcheRows) {
			PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
			List<String> errors = new LinkedList<String>();
			String[] tokens = options.split(",");
			
			if ("can".equals(canOrCannot.toLowerCase()) || canOrCannot.isEmpty()) {
				for (String token : tokens) {
					JsonNode node = null;
					try {
						node = this.dataProvider.getDataJson(token);	
					} catch (Exception e) {
						//meaning actual option is provided, so accept exception
					}
					
					if (node != null) {
						Iterator<JsonNode> it = node.get("options").iterator();
						while (it.hasNext()) {
							String option = it.next().asText();
							if (!table.seeActionDropdownOption(matchRow, option)) {
								errors.add(option);
							}
						}
					} else {
						String option = token.trim();
						if (!table.seeActionDropdownOption(matchRow, option)) {
							errors.add(option);
						}
					}
				}
				if (errors.size() > 0) {
					throw new BrowserException(String.format("option[%s] should be available in 'action' dropdown in [%s] outter row", StringUtils.join(errors, ","), i));	
				}
			} else if ("cannot".equals(canOrCannot.toLowerCase())) {
				for (String token : tokens) {
					JsonNode node = null;
					try {
						node = this.dataProvider.getDataJson(token);	
					} catch (Exception e) {
						//meaning actual option is provided, so accept exception
					}
					
					if (node != null) {
						Iterator<JsonNode> it = node.get("options").iterator();
						while (it.hasNext()) {
							String option = it.next().asText();
							if (table.seeActionDropdownOption(matchRow, option)) {
								errors.add(option);
							}
						}
					} else {
						String option = token.trim();
						if (table.seeActionDropdownOption(matchRow, option)) {
							errors.add(option);
						}
					}
				}
				if (errors.size() > 0) {
					throw new BrowserException(String.format("option[%s] should not be available in 'action' dropdown in [%s] outter row", StringUtils.join(errors, ","), i));	
				}
			}
			i ++;
		}
	}
	
		
	@And("^I ?(|can|cannot) see 'note' (?:input) in following rows? in '?message'? table in patients to keep table$")
	public void see_note_input_in_following_row_in_message_table(List<WebElement> outterRows, String canOrCannot, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		for (WebElement matchRow : matcheRows) {
			if ("can".equals(canOrCannot.toLowerCase()) || canOrCannot.isEmpty()) {
				if (!table.seeNoteInput(matchRow)) {
					throw new BrowserException("'note' input is not visible when it should be");
				}
			} else if ("cannot".equals(canOrCannot.toLowerCase())) {
				if (table.seeNoteInput(matchRow)) {
					throw new BrowserException("'note' input is visible when it should not be");
				}
			}
		}
	}
	

	/**
	 * Type 'y' in 'x' field/text/textarea in following rows in 'yyyy' table (from outter table)
	 */
	@And("^I type \"?(.*?)\"? (?:in|into) 'note' (?:text|textarea|field) in following rows? in '?message'? table in patients to keep table$")
	public void type_into_note_input_in_following_row_in_message_table(List<WebElement> outterRows, String text, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		for (WebElement matchRow : matcheRows) {
			PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
			table.typeNoteInput(matchRow, text);
		}
	}
		
	
	/**
	 * See 'y' is displayed in 'x' field/text/textarea in following rows in 'yyyy' table (from outter table)
	 */
	@And("^I see \"?(.*?)\"? is displayed in 'note' (?:text|textarea|field) in following rows? in '?message'? table in patients to keep table$")
	public void see_is_displayed_in_note_input_in_following_row_in_message_table(List<WebElement> outterRows, String expectedText, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		for (WebElement matchRow : matcheRows) {
			PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
			String actText = table.getNoteInputTextValue(matchRow);
			if (!actText.contains(expectedText)) {
				throw new BrowserException("'note' input field[" + actText + "] does not contain ["+expectedText + "]");
			}
		}
		
	}
	
	
	/**
	 * can/cannot see 'x' icon in following rows in 'yyyy' table
	 */
	@And("^I ?(|can|cannot) see 'add office comments' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void see_add_office_comments_icon_in_following_row_in_message_table(List<WebElement> outterRows, String canOrCannot, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		for (WebElement matchRow : matcheRows) {
			if ("can".equals(canOrCannot.toLowerCase()) || canOrCannot.isEmpty()) {
				if (!table.seeAddOfficeCommentsIcon(matchRow)) {
					throw new BrowserException("'add office comments' icon is not visible when it should be");
				}
			} else if ("cannot".equals(canOrCannot.toLowerCase())) {
				if (table.seeAddOfficeCommentsIcon(matchRow)) {
					throw new BrowserException("'add office comments' icon is visible when it should not be");
				}
			}
		}
		
	}
	
	/**
	 * click 'x' icon in following rows in 'yyyy' table
	 */
	@And("^I click 'add office comments' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void click_add_office_comments_icon_in_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		
		table.clickAddOfficeCommentsIcon(matcheRows.get(0));
	}
	
	/**
	 * if visible, click 'x' icon in following rows in 'yyyy' table
	 */
	@And("^if visible, I click 'add office comments' (?:button|icon|link|hyperlink) in following rows? in '?message'? table in patients to keep table$")
	public void if_visible_click_add_office_comments_icon_in_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
		List<WebElement> matcheRows = null;
		try {
			matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		} catch (BrowserException e) {
			//occur when no row can be found
			return;
		} catch (TimeoutException e) {
			//occur when table object doesn't exist
			return;
		}
		if (matcheRows.size() <= 0) {
			return;
		}
		
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		if (table.seeAddOfficeCommentsIcon(matcheRows.get(0))) {
			
			table.clickAddOfficeCommentsIcon(matcheRows.get(0));
			
		}
		
	}

	/**
	 * see following rows in 'y' table
	 */
	@And("^I see following rows? in '?message'? table in patients to keep table$")
	public List<WebElement> see_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
	
		List<WebElement> matchedRows = new LinkedList<WebElement>();
		StringBuilder errors = new StringBuilder();
		int i = 0;
		for (WebElement outterRow : outterRows) {
			PharmacyMessagesSearchResultTable table = null;
			try {
				table = new PharmacyMessagesSearchResultTable(getBrowser(), outterRow);
			} catch (TimeoutException e) {
				//case where table doesn't exist
				errors.append(String.format("inner table of outter row[no. %s] isn't visible!\n", i));
				continue;
			}
		
		
			List<Map<String, String>> rows2 = rows.asMaps(String.class, String.class);
			for (Map<String, String> row : rows2) {
				
				Map<String, String> critera = new HashMap<String, String>();
				
				for (Entry<String, String> entry : row.entrySet()) {
	//					System.out.println("{Processing KEY} "+key);
						String value = ExpressionHandler.resolve(entry.getValue());
						switch (entry.getKey()) {
							//case "patientName": {
							//	critera.put(PrescriptionSearchResultTable.COL_NAME_PATIENT_NAME, entry.getValue());			break;
							//}
						case "patientSummary": {
							critera.put(PharmacyMessagesSearchResultTable.PatientSummaryColumn, value);			break;
						}
						case "medicationDispensed": {
							critera.put(PharmacyMessagesSearchResultTable.MedicationDispensedColumn, value);			break;
						}
						case "actions": {
							critera.put(PharmacyMessagesSearchResultTable.ActionsColumn, value);			break;
						}

						}
						
				}
	
				//return directly if no critera is provided
				if(critera.size() <= 0) {
					return outterRows;
				}
				
				List<WebElement> rowEles = table.findAllRows(critera, false);
				if (rowEles.size() <= 0) {
					errors.append(String.format(" outter row[no. %s] does not match critera[%s]\n", i, critera));
				} else {
					matchedRows.addAll(rowEles);
				}
			}
			i ++;
		}
		
		if (matchedRows.size() <= 0 && errors.toString().length() > 0) {
			throw new BrowserException(errors.toString());
		}
		
		return matchedRows;
			
	}
	
	
	/**
	 * cannot see following rows in 'y' table
	 */
	@And("^I cannot see following rows? in '?message'? table in patients to keep table$")
	public void cannot_see_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
		try {
			this.see_following_row_in_message_table(outterRows, rows);
		} catch (BrowserException e) {
			return;
		}
		throw new BrowserException(String.format("no row should matches critera[%s]\n", rows.toString()));

	}

	/**
	 * click following row in 'y' table
	 */
	@And("^I click following rows? in '?message'? table in patients to keep table$")
	public void click_following_row_in_message_table(List<WebElement> outterRows, DataTable rows) {
		List<WebElement> matcheRows = this.see_following_row_in_message_table(outterRows, rows);
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		
		table.clickRow(matcheRows.get(0));
	}
	
	/**
	 * can or cannot see rows in 'y' table
	 */
	@And("I (|can|cannot) see rows in '?message'? table in patients to keep table")
	public void can_or_cannot_see_rows_in_message_table(String canOrCannot) {
		
		PharmacyMessagesSearchResultTable table = new PharmacyMessagesSearchResultTable(getBrowser());
		List<WebElement> allRows = table.getAllRows();
		if (canOrCannot.trim().equalsIgnoreCase("can")) {
			if (! allRows.isEmpty()) {
				throw new BrowserException(String.format("rows are expected to be empty, but rows[%s] exists!", StringUtils.join(allRows, ',')));
			}
		} else {
			if (allRows.isEmpty()) {
				throw new BrowserException(String.format("rows are expected to exist, but rows is empty!!"));
			}
		}
	}

	

}
