package org.gradle;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class Printer {
	
	private static Logger logger = Logger.getLogger(Printer.class);

	static private final String BASE_OUTPUT_PATH= "build/codegen";
	static {
		Properties p = new Properties();
		p.setProperty("file.resource.loader.path", "src/main/resources");
        Velocity.init(p);
	}
	private Component[] holder;
	
	public Printer(Component[] components) {
		this.holder = components;
	}
	
	public String print() throws IOException {
		try {
			FileUtils.forceDelete(new File(BASE_OUTPUT_PATH));	
		} catch (Exception e) {
			//ignore
		}
		
		
        StringBuilder sb = new StringBuilder();
        
        for (int i  = 0 ; i < holder.length ; i ++) {
	        for (Component subComponent : holder[i].getComponents()) {
	        	
	            String result = printObject(subComponent);
		        sb.append(result);
		        result = printStep(subComponent);
		        sb.append(result);
	        }
        }

        return sb.toString();
	}

	public String printObject(Component component) {
		VelocityContext context = new VelocityContext();
		StringWriter w = new StringWriter();
		/* lets make a Context and put data into it */

		context.put("className", StringUtils.capitalize(component.getName()));
		context.put("titleName", component.getTitle());
		context.put("loggerClassName", context.get("className") + ".class");
		
		//locator field initiation
		VelocityContext context1 = new VelocityContext();
		StringWriter w1 = new StringWriter();
		context1.put("fieldName", component.getType() + "Locator");
		context1.put("locatorStrategy", "id");
		context1.put("locatorValue", "fillme");
		String locator = component.getLocator();
		if (locator != null) {
			String[] tokens = locator.split("=");
			if (tokens.length == 2) {
				context1.put("locatorStrategy", tokens[0].trim());
				context1.put("locatorValue", tokens[1].trim());					
			}
		}
		Velocity.mergeTemplate("fieldsObjectLocator.vm", "UTF-8", context1, w1);
		Component[] subcomponents = component.getComponents();
		for (Component subcomponent : subcomponents) {
			printField(context1, w1, subcomponent);
			
			for (Component icon : subcomponent.getComponents()) {
				printField(context1, w1, icon);
			}
		}
		
		context.put("subcomponentLocatorFields", w1.toString());
		
		//table column name field initialization
		VelocityContext context3 = new VelocityContext();
		StringWriter w3 = new StringWriter();
		for (Component subcomponent : subcomponents) {
			context3.put("fieldName", StringUtils.capitalize(subcomponent.getName()) 
					+ StringUtils.capitalize(subcomponent.getType()));
			context3.put("fieldValue", subcomponent.getName());
			Velocity.mergeTemplate("fieldsObjectString.vm", "UTF-8", context3, w3);
			
			for (Component icon : subcomponent.getComponents()) {
				context3.put("fieldName", StringUtils.capitalize(icon.getName()) 
						+ StringUtils.capitalize(icon.getType()));
				context3.put("fieldValue", icon.getName());
				Velocity.mergeTemplate("fieldsObjectString.vm", "UTF-8", context3, w3);
			}
		}
		
		context.put("subcomponentColumnFields", w3.toString());
		
		//table column schema setup initialization
		if ("table".equals(component.getType())) {
			VelocityContext context4 = new VelocityContext();
			StringWriter w4 = new StringWriter();
			for (Component subcomponent : subcomponents) {
				if ("row".equals(subcomponent.getType())) {
					continue;
				}
				context4.put("columnField", StringUtils.capitalize(subcomponent.getName()) 
						+ StringUtils.capitalize(subcomponent.getType()));
				context4.put("locatorField", StringUtils.capitalize(subcomponent.getName()) 
						+ StringUtils.capitalize(subcomponent.getType()) + "Locator");
				Velocity.mergeTemplate("methodCallsObjectTableColumnSetup.vm", "UTF-8", context4, w4);
				for (Component icon : subcomponent.getComponents()) {
					context4.put("locatorField", StringUtils.capitalize(icon.getName()) 
							+ StringUtils.capitalize(icon.getType()) + "Locator");
					context4.put("iconField", StringUtils.capitalize(icon.getName()) 
							+ StringUtils.capitalize(icon.getType()));
					Velocity.mergeTemplate("methodCallsObjectTableColumnsIconSetup.vm", "UTF-8", context4, w4);
				}
			}
			context.put("columnsSetupCall", w4.toString());
		}
		
		//table column/icon method declaration statement
		if ("table".equals(component.getType())) {
			VelocityContext context5 = new VelocityContext();
			StringWriter w5 = new StringWriter();
			context5.put("toClassName", component.getTo());
			
			if (subcomponents.length == 1) {
				Component subcomponent = subcomponents[0];
				context5.put("columnField", StringUtils.capitalize(subcomponent.getName()) 
						+ StringUtils.capitalize(subcomponent.getType()));
				context5.put("toClassName", subcomponent.getTo());
			} else {
				for (Component subcomponent : subcomponents) {
					if (subcomponent.getComponents().length <= 0) {
						if ("row".equalsIgnoreCase(subcomponent.getType())) {
							context5.put("toClassName", subcomponent.getTo());
						} else {
						context5.put("columnField", StringUtils.capitalize(subcomponent.getName()) 
								+ StringUtils.capitalize(subcomponent.getType()));
						}
					}
				}
			}

			
			Velocity.mergeTemplate("methodsObjectTable_Row.vm", "UTF-8", context5, w5);
			
			for (Component subcomponent : subcomponents) {
				if ("table".equals(subcomponent.getType())) {
					continue;
				}
				
				for (Component icon : subcomponent.getComponents()) {
					context5.put("toClassName", icon.getTo());
					context5.put("itemMethodCall", StringUtils.capitalize(icon.getName()) + StringUtils.capitalize(icon.getType()));
					context5.put("columnField", StringUtils.capitalize(icon.getName()) 
							+ StringUtils.capitalize(icon.getType()));
					switch(icon.getType()) {
						case "input": {
							Velocity.mergeTemplate("methodsObjectTable_Input.vm", "UTF-8", context5, w5 );
							break;
						}
						case "icon": {
							Velocity.mergeTemplate("methodsObjectTable_Icon.vm", "UTF-8", context5, w5);
							break;
						}
						case "checkbox": {
							Velocity.mergeTemplate("methodsObjectTable_Checkbox.vm", "UTF-8", context5, w5);
							break;
						}
						case "dropdown": {
							Velocity.mergeTemplate("methodsObjectTable_Dropdown.vm", "UTF-8", context5, w5 );
							break;
						}
						case "text": {
							Velocity.mergeTemplate("methodsObjectTable_Text.vm", "UTF-8", context5, w5 );
							break;
						}
					}
				}
			}
			context.put("columnsEventMethods", w5.toString());
		}
		
		
		//method declaration statement
		VelocityContext context2 = new VelocityContext();
		StringWriter w2 = new StringWriter();
		for (Component subcomponent : subcomponents) {
    		initCtxForObjectMethodCall(context2, subcomponent);
			printObjectMethodCall(context2, w2, subcomponent);
			Velocity.evaluate( context2, w2, "mystring", "");
		}
		context.put("subcomponentMethods", w2.toString());
		
		
		/* lets render a template */
		switch (component.getType()) {
			case "widget": {
				Velocity.mergeTemplate("widgetObject.vm", "UTF-8", context, w );
				break;
			}
			case "section": {
				Velocity.mergeTemplate("sectionObject.vm", "UTF-8", context, w );
				break;
			}
			case "component": {
				Velocity.mergeTemplate("sectionObject.vm", "UTF-8", context, w );
				break;
			}
			case "table": {
				Velocity.mergeTemplate("tableObject.vm", "UTF-8", context, w );
				break;
			}
			case "overlay": {
				Velocity.mergeTemplate("overlayObject.vm", "UTF-8", context, w );
				break;
			}
			case "page": {
				Velocity.mergeTemplate("pageObject.vm", "UTF-8", context, w );
				break;
			}
			default : {
				throw new UnsupportedOperationException(component.getType() + " isn't supported!");
			}
		}
		//System.out.println(" template : " + w );

		/* lets make our own string to render */

		Velocity.evaluate( context, w, "mystring", "");
		String result = w.toString();
		
		try {
			File file = new File(String.format(BASE_OUTPUT_PATH + "/object/%s/%s.java", component.getParentPathName(), context.get("className")));
			logger.info(String.format("save object file[%s]", file.getAbsolutePath()));
			FileUtils.writeStringToFile(file, result);
		} catch (IOException e) {
			throw new IllegalStateException("Unable to save object!", e);
		}
		return result;
	}

	private void initCtxForObjectMethodCall(VelocityContext context2,
			Component subcomponent) {
		context2.put("name", subcomponent.getName());
		context2.put("type", subcomponent.getType());
		context2.put("itemMethodCall", StringUtils.capitalize(subcomponent.getName()) + StringUtils.capitalize(subcomponent.getType()));
		context2.put("locator", StringUtils.capitalize(subcomponent.getName()) 
				+ StringUtils.capitalize(subcomponent.getType()) + "Locator");
		context2.put("toClassName", subcomponent.getTo());
	}

	private void printObjectMethodCall(VelocityContext context2,
			StringWriter w2, Component subcomponent) {
		switch (subcomponent.getType()) {
			case "text": { 
				Velocity.mergeTemplate("methodsObjectText.vm", "UTF-8", context2, w2 );
			break;}
			case "input": { 
				Velocity.mergeTemplate("methodsObjectInput.vm", "UTF-8", context2, w2 );
			break;}
			case "field": { 
				Velocity.mergeTemplate("methodsObjectInput.vm", "UTF-8", context2, w2 );
			break;}
			case "dropdown": { 
				Velocity.mergeTemplate("methodsObjectDropdown.vm", "UTF-8", context2, w2 );
			break;}
			case "button": { 
				Velocity.mergeTemplate("methodsObjectClick.vm", "UTF-8", context2, w2 );
			break;}
			case "icon": { 
				Velocity.mergeTemplate("methodsObjectClick.vm", "UTF-8", context2, w2 );
			break;}
			case "link": { 
				Velocity.mergeTemplate("methodsObjectClick.vm", "UTF-8", context2, w2 );
			break;}
			case "checkbox": { 
				Velocity.mergeTemplate("methodsObjectCheckbox.vm", "UTF-8", context2, w2 );
			break;}
			case "radiobutton": { 
				Velocity.mergeTemplate("methodsObjectRadioButton.vm", "UTF-8", context2, w2 );
			break;}
			case "table": { 
				Velocity.mergeTemplate("methodsObjectTable.vm", "UTF-8", context2, w2 );
			break;}
			case "column": { 
				Velocity.mergeTemplate("methodsObjectTable.vm", "UTF-8", context2, w2 );
			break;}
			case "row": { 
				//Velocity.mergeTemplate("methodsObjectTable.vm", "UTF-8", context2, w2 );
			break;}	
			case "component": { 
				Velocity.mergeTemplate("methodsObjectTable.vm", "UTF-8", context2, w2 );
			break;}
			case "overlay": { 
				Velocity.mergeTemplate("methodsObjectTable.vm", "UTF-8", context2, w2 );
			break;}
			case "section": { 
				Velocity.mergeTemplate("methodsObjectTable.vm", "UTF-8", context2, w2 );
			break;}
			case "page": { 
				Velocity.mergeTemplate("methodsObjectTable.vm", "UTF-8", context2, w2 );
			break;}	
			default : {
				throw new UnsupportedOperationException(subcomponent.getType() + " isn't supported!");
			}
		}
	}

	
	private void printField(VelocityContext context1, StringWriter w1,
			Component icon) {
		String locator;
		context1.put("fieldName", StringUtils.capitalize(icon.getName()) 
				+ StringUtils.capitalize(icon.getType()) + "Locator");
		
		context1.put("locatorStrategy", "id");
		context1.put("locatorValue", "fillme");
		locator = icon.getLocator();
		if (locator != null) {
			String[] tokens = locator.split("=");
			if (tokens.length == 2) {
				context1.put("locatorStrategy", tokens[0].trim());
				context1.put("locatorValue", tokens[1].trim());					
			}
		}
		Velocity.mergeTemplate("fieldsObjectLocator.vm", "UTF-8", context1, w1);
	}
	
	public String printStep(Component component) {
		VelocityContext context = new VelocityContext();
		String name = component.getName();
		String stepClassName = getStepName(name);
		context.put("stepClassName", stepClassName);
		context.put("className", name);
		
		Component owner = component.findOwner(name);
		context.put("currentContext", component.getContext());
		context.put("currentContextType", component.getType());
		context.put("currentClassName", name);
		if (owner != null) {
			context.put("ownerContext", owner.getContext() + " " + owner.getType());
			context.put("ownerClassName", owner.getName());
		} else {
			//context.put("ownerContext", context.get(component.getType() + "Context") + " " + component.getType());
			context.put("ownerClassName", context.get(component.getType() + "ClassName"));
		}
		
		StringWriter w = new StringWriter();
		/* lets make a Context and put data into it */

		//method declaration statement
		VelocityContext context2 = new VelocityContext();
		context2.put("className", context.get("className"));
		context2.put("ownerContext", context.get("ownerContext"));
		context2.put("ownerClassName", context.get("ownerClassName"));
		context2.put("currentContext", context.get("currentContext"));
		context2.put("currentClassName", context.get("currentClassName"));
		context2.put("currentContextType", context.get("currentContextType"));
		context2.put("stepClassName", context.get("stepClassName"));
		
		if (context2.get("currentContext") != null) {
			String val = (String) context2.get("currentContext");
			context2.put("currentMethod", getMethodCompatibleName(val));	
		}
		
		StringWriter caseStatementsForAssertingFollowingObject = new StringWriter();
		StringWriter caseStatementsForPopulatingFollowingObject = new StringWriter();
		StringWriter caseStatementsForSeeFollowingTable = new StringWriter();
		StringWriter w2 = new StringWriter();
		//System.out.println(component.getName());
		for (Component subcomponent : component.getComponents()) {
    		context2.put("name",  subcomponent.getName());
    		context2.put("itemContext",      		
    				StringUtils.join(
    						StringUtils.splitByCharacterTypeCamelCase(StringUtils.capitalize(subcomponent.getName())),
    						' ').toLowerCase());
    		context2.put("itemMethod", StringUtils.join(
					StringUtils.splitByCharacterTypeCamelCase(StringUtils.capitalize(subcomponent.getName())  + StringUtils.capitalize(subcomponent.getType())) ,
					'_').toLowerCase());
    		context2.put("type", subcomponent.getType());
    		context2.put("itemMethodCall", StringUtils.capitalize(subcomponent.getName()) + StringUtils.capitalize(subcomponent.getType()));
			context2.put("locator", StringUtils.capitalize(subcomponent.getName()) 
					+ StringUtils.capitalize(subcomponent.getType()) + "Locator");
    		context2.put("toClassName", subcomponent.getTo());
    		//System.out.println(subcomponent.getType());
			switch (subcomponent.getType()) {
				case "text": {
					
					//populate getter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "see_is_displayed_in_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Getter.vm", "UTF-8", context2, caseStatementsForAssertingFollowingObject);
					
			    	Velocity.mergeTemplate("methodsStepsText.vm", "UTF-8", context2, w2 );
				break;}
				case "input": {
					//populate getter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "see_is_displayed_in_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Getter.vm", "UTF-8", context2, caseStatementsForAssertingFollowingObject);
					
					//populate setter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "type_into_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Setter.vm", "UTF-8", context2, caseStatementsForPopulatingFollowingObject);
					
		    		Velocity.mergeTemplate("methodsStepsInput.vm", "UTF-8", context2, w2 );
				break;}
				case "field": { 

					//populate getter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "see_is_displayed_in_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Getter.vm", "UTF-8", context2, caseStatementsForAssertingFollowingObject);
					
					//populate setter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "type_into_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Setter.vm", "UTF-8", context2, caseStatementsForPopulatingFollowingObject);
					
		    		Velocity.mergeTemplate("methodsStepsInput.vm", "UTF-8", context2, w2 );
				break;}
				case "dropdown": {
					//populate getter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "see_is_selected_in_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Getter.vm", "UTF-8", context2, caseStatementsForAssertingFollowingObject);

					
					//populate setter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "select_from_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Setter.vm", "UTF-8", context2, caseStatementsForPopulatingFollowingObject);
					
		    		Velocity.mergeTemplate("methodsStepsDropdown.vm", "UTF-8", context2, w2 );
				break;}
				case "button": { 
		    		Velocity.mergeTemplate("methodsStepsClick.vm", "UTF-8", context2, w2 );
				break;}
				case "icon": { 
		    		Velocity.mergeTemplate("methodsStepsClick.vm", "UTF-8", context2, w2 );
				break;}
				case "link": { 
		    		Velocity.mergeTemplate("methodsStepsClick.vm", "UTF-8", context2, w2 );
				break;}
				case "checkbox": {
					
					//populate getter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "see_is_selected_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Checkbox_Getter.vm", "UTF-8", context2, caseStatementsForAssertingFollowingObject);
					
					//populate setter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "check_uncheck_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Checkbox_Setter.vm", "UTF-8", context2, caseStatementsForPopulatingFollowingObject);
					
		    		Velocity.mergeTemplate("methodsStepsCheckbox.vm", "UTF-8", context2, w2 );
				break;}
				case "radiobutton": {
					//populate getter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "see_is_selected_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Checkbox_Getter.vm", "UTF-8", context2, caseStatementsForAssertingFollowingObject);
	
					
					//populate setter
					context2.put("componentName", subcomponent.getName());
					context2.put("stepMethodName", "check_uncheck_" + context2.get("itemMethod"));
					Velocity.mergeTemplate("methodsStepsObject_CriteraConditionStatement_Checkbox_Setter.vm", "UTF-8", context2, caseStatementsForPopulatingFollowingObject);
					
		    		Velocity.mergeTemplate("methodsStepsRadioButton.vm", "UTF-8", context2, w2 );
				break;}
				case "table": {
					if (! component.getType().equals("table")) {
						break;
					}
					//if is outter table
					Map<String, String> critera = new HashMap<String, String>();
					critera.put("type", "table");
					//System.out.println(component.getName());
					Component innerTable = component.findChild(critera);
					if (innerTable != null) {
						context2.put("innerTableStepClassName", this.getStepName(innerTable.getName()));
						context2.put("innerTableMethodCompatibleContext",  this.getMethodCompatibleName(innerTable.getContext()));
						for (Component colComponent : innerTable.getComponents()) {
							for (Component iconComponent : colComponent.getComponents()) {
								initCtxForTableIcon(context2, iconComponent);
								
								switch (iconComponent.getType()) {
									case "input": {
										Velocity.mergeTemplate("methodsStepsTable_InnerTable_Input.vm", "UTF-8", context2, w2 );
										break;
									}
									case "icon": {
										Velocity.mergeTemplate("methodsStepsTable_InnerTable_Icon.vm", "UTF-8", context2, w2 );
										break;
									}
									case "dropdown": {
										Velocity.mergeTemplate("methodsStepsTable_InnerTable_Dropdown.vm", "UTF-8", context2, w2 );
										break;
									}
									case "checkbox": {
										Velocity.mergeTemplate("methodsStepsTable_InnerTable_Checkbox.vm", "UTF-8", context2, w2 );
										break;
									}
									case "text": {
										Velocity.mergeTemplate("methodsStepsTable_InnerTable_Text.vm", "UTF-8", context2, w2 );
										break;
									}
								}
							}
							
						}
					}
					break;
				}
				case "component": {
					break;
				}
				case "overlay": {
					break;
				}
				case "page": {
					break;
				}
				case "section": {
					break;
				}
				case "row": {
					break;
				}
				case "column": {
					
/*					if (defaultRowMtdDeclaration.toString().isEmpty()) {
						context2.put("tableClassName", component.getName());
						context2.put("columnName", component.getName());
						context2.put("columnField", component.getName());
						//context2.put("widgetClassName", component.getName());
						for (Component icon : subcomponent.getComponents()) {
							Velocity.mergeTemplate("methodsStepsTable_CriteraConditionStatement.vm", "UTF-8", context2, defaultRowMtdDeclaration);
						}
						context2.put("columnCaseStatements", defaultRowMtdDeclaration.toString());
						Velocity.mergeTemplate("methodsStepsTable_seeOrNotSeeRows.vm", "UTF-8", context2, w2 );
					}*/
					//case generation
				//	if (defaultRowMtdDeclaration.toString().isEmpty()) {
						context2.put("tableClassName", name);
						context2.put("columnName", subcomponent.getName());
						context2.put("columnField", StringUtils.capitalize(subcomponent.getName()) 
								+ StringUtils.capitalize(subcomponent.getType()));
						
						//context2.put("widgetClassName", component.getName());
						Velocity.mergeTemplate("methodsStepsTable_CriteraConditionStatement.vm", "UTF-8", context2, caseStatementsForSeeFollowingTable);
				//	}
					
					for (Component icon : subcomponent.getComponents()) {
						initCtxForTableIcon(context2, icon);
						
						//if is inner table
						//System.out.println(component.getName());
						if (owner != null && "table".equalsIgnoreCase(owner.getType())) {
							switch (icon.getType()) {
								case "input": {
									Velocity.mergeTemplate("methodsStepsTableInner_Input.vm", "UTF-8", context2, w2 );
									break;
								}
								case "icon": {
									Velocity.mergeTemplate("methodsStepsTableInner_Icon.vm", "UTF-8", context2, w2 );
									break;
								}
								case "dropdown": {
									Velocity.mergeTemplate("methodsStepsTableInner_Dropdown.vm", "UTF-8", context2, w2 );
									break;
								}
								case "checkbox": {
									Velocity.mergeTemplate("methodsStepsTableInner_Checkbox.vm", "UTF-8", context2, w2 );
									break;
								}
								case "text": {
									Velocity.mergeTemplate("methodsStepsTableInner_Text.vm", "UTF-8", context2, w2 );
									break;
								}
							}
						} else {
							switch (icon.getType()) {
								case "input": {
									Velocity.mergeTemplate("methodsStepsTable_Input.vm", "UTF-8", context2, w2 );
									break;
								}
								case "icon": {
									Velocity.mergeTemplate("methodsStepsTable_Icon.vm", "UTF-8", context2, w2 );
									break;
								}
								case "checkbox": {
									Velocity.mergeTemplate("methodsStepsTable_Checkbox.vm", "UTF-8", context2, w2 );
									break;
								}
								case "text": {
									Velocity.mergeTemplate("methodsStepsTableInner_Text.vm", "UTF-8", context2, w2 );
									break;
								}
								default: {
									Velocity.mergeTemplate("methodsStepsTable_Icon.vm", "UTF-8", context2, w2 );
									break;
								}
							}
						}
					}
					break;
				}
				default : {
					throw new UnsupportedOperationException(subcomponent.getType() + " isn't supported!");
				}
			}
			Velocity.evaluate( context2, w2, "mystring", "");
		}
		if ("table".equals(component.getType())) {
			Component[] subcomponents = component.getComponents();
			if (subcomponents.length == 1) {
				Component subcomponent = subcomponents[0];
				context2.put("toClassName", subcomponent.getTo());
			} else {
				for (Component subcomponent : subcomponents) {
					if (subcomponent.getComponents().length <= 0) {
						if ("row".equals(subcomponent.getType())) {
							context2.put("toClassName",  subcomponent.getTo());	
						}
						break;
					}
				}
			}
			context2.put("columnCaseStatements", caseStatementsForSeeFollowingTable.toString());
			//if is inner table
			if (owner != null && "table".equalsIgnoreCase(owner.getType())) {
				Velocity.mergeTemplate("methodsStepsTableInner_default.vm", "UTF-8", context2, w2 );
			} else {
				
				Map<String, String> critera = new HashMap<String, String>();
				critera.put("type", "table");
				Component innerTable = component.findChild(critera);
				StringWriter innerTableSeeFollowingTableStatement = new StringWriter();
				if (innerTable != null) {
					//if is outter table
					context2.put("innerTableStepClass", this.getStepName(innerTable.getName()));
					Velocity.mergeTemplate("methodsStepsTableInner_SeeFollowingRowMethodStatement.vm", "UTF-8", context2, innerTableSeeFollowingTableStatement);
				}
				context2.put("innerTableSeeFollowingRowMethodStatement", innerTableSeeFollowingTableStatement.toString());
				Velocity.mergeTemplate("methodsStepsTable_default.vm", "UTF-8", context2, w2 );
			}
		}
		if ("widget".equals(component.getType()) || "component".equals(component.getType()) || "overlay".equals(component.getType())  || "section".equals(component.getType())  || "page".equals(component.getType()) ) {
			Component[] subcomponents = component.getComponents();
			if (subcomponents.length == 1) {
				Component subcomponent = subcomponents[0];
				context2.put("toClassName", subcomponent.getTo());
			} else {
				for (Component subcomponent : subcomponents) {
					if (subcomponent.getComponents().length <= 0) {
						context2.put("toClassName",  subcomponent.getTo());
						break;
					}
				}
			}
			context2.put("caseStatementsForAssertingFollowingObject", caseStatementsForAssertingFollowingObject.toString());
			context2.put("caseStatementsForPopulatingFollowingObject", caseStatementsForPopulatingFollowingObject.toString());
			Velocity.mergeTemplate("methodsStepsObject_default.vm", "UTF-8", context2, w2 );
		}
		
		
		context.put("stepMethods", w2.toString());

		Velocity.mergeTemplate("stepsObject.vm", "UTF-8", context, w );

		/* lets make our own string to render */

		Velocity.evaluate( context, w, "mystring", "");
		String result = w.toString();
		
		try {
			File file = new File(String.format(BASE_OUTPUT_PATH + "/step/%s/%s.java", component.getParentPathName(), context.get("stepClassName")));
			logger.info(String.format("save step file[%s]", file.getAbsolutePath()));
			FileUtils.writeStringToFile(file, result);
		} catch (IOException e) {
			throw new IllegalStateException("Unable to save step!", e);
		}
		return result;
	}

	private String getMethodCompatibleName(String val) {
		return val.trim().replace(' ', '_');
	}

	private void initCtxForTableIcon(VelocityContext context2, Component icon) {
		context2.put("toClassName", icon.getTo());
		context2.put("itemContext",      		
				StringUtils.join(
						StringUtils.splitByCharacterTypeCamelCase(StringUtils.capitalize(icon.getName())),
						' ').toLowerCase());
		context2.put("itemMethod", StringUtils.join(
				StringUtils.splitByCharacterTypeCamelCase(StringUtils.capitalize(icon.getName())  + StringUtils.capitalize(icon.getType())) ,
				'_').toLowerCase());
		context2.put("iconMethodName",  StringUtils.capitalize(icon.getName()) + StringUtils.capitalize(icon.getType()));
	}

	private String getStepName(String name) {
		String stepClassName;
		String[] tokens = StringUtils.splitByCharacterTypeCamelCase(name);
		stepClassName = tokens[tokens.length - 1];
		for (int i = 0 ; i < tokens.length - 1 ; i ++) {
			stepClassName += tokens[i];
		}
		stepClassName += "Step";
		return stepClassName;
	}
}
