package org.gradle;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class Component {
	
	private Component parent;
	
    private JSONObject object;
    
    private String path;
    
    private File file;

    public Component(File file) throws IOException {
        this(FileUtils.readFileToString(file, "UTF-8"));
        
        this.file = file;
    }
    
    public Component(String content) {
        this.object = new JSONObject(content);    	
    }
    
    public Component(JSONObject object) {
        this.object = object;    	
    }
    
    public void setParentPathName(String path) {
    	if (path == null) {
    		throw new NullPointerException("path cnanot be null!");
    	}
    	this.path = path;
    }
    
    public String getParentPathName() {
    	
    	if (this.path == null) {
    		if (this.parent == null) {
    			//throw new IllegalStateException("can't find path!");
    			return ".";
    		}
    		return this.parent.getParentPathName();
    	}
    	return this.path;
    }

    public String getType() {
    	return this.object.getString("type");
    }
    
	public String getContext() {
		return this.object.getString("context").trim();
	}

	
    public String getName() {
    	return this.object.getString("name").trim();
    }
    
	public String getTitle() {
		return this.object.getString("title").trim();
	}
    
    public String getTo() {
    	if (this.object.has("to")) {
    		return this.object.getString("to").trim();
    	} else {
    		Component component = this.getParent();
    		while (component.getParent() != null) {
    			if (component.getParent().getParent() == null) {
    				break;
    			} else {
    				component = component.getParent();
    			}
    		}
    		try {
    			return component.getName();	
    		} catch (Exception e) {
    			return this.getName();
    		}
    	}
    }
    
    public String getLocator() {
    	if (this.object.has("locator")) {
    		return this.object.getString("locator").trim();
    	} else {
    		return null;
    	}
    }
    
	public Component[] getComponents() {
    	List<Component> components = new ArrayList<Component>();
    	try {
    		JSONArray jsonArray = this.object.getJSONArray("components");
        	for (int i = 0 ; i < jsonArray.length() ; i ++) {
        		Component subcomponent = new Component(jsonArray.getJSONObject(i));
        		subcomponent.setParent(this);
    			components.add(subcomponent);
        	}
    		return components.toArray(new Component[]{});
    	} catch (Exception e) {
    		return new Component[] {};
    	}
    	
    }
    
    public Component getToComponent() {
    	
    	return resolve(this.getTo());
    }

	public Component resolve(String targetName) {
		Component holder = this;
    	if (this.parent != null) {
    		holder = this.parent;
    	}
    	
    	for (Component pageLevelComponent : holder.getComponents()) {
    		if (pageLevelComponent.getName().toLowerCase().equals(targetName.toLowerCase())) {
    			return pageLevelComponent;
    		}
    	}
    	throw new IllegalStateException("Unable to resolve component[name=" + targetName + "]");
	}

	public void setParent(Component parent2) {
		this.parent = parent2;
	}
	
	public Component getParent() {
		return this.parent;
	}

	public Component findOwner(String name) {
		Component holder = this;
		while (holder.getParent() != null) {
			holder = holder.getParent();
		}
		
		for (Component component : holder.getComponents()) {
			for (Component subcomponent : component.getComponents()) {
				String subcomponentName = subcomponent.getName();
				if (subcomponentName.equals(name)) {
					return component;					
				}
			}
		}
		return null;
	}
	
	public Component findChild(Map<String, String> critera) {
		for (Component subcomponent : this.getComponents()) {
			Component targetChild = this.findChild(subcomponent, critera);
			if (targetChild != null) {
				return targetChild;
			}
		}
		return null;
	}
	
	public Component findChild(Component component, Map<String, String> critera) {
		
		//match first
		Component targetChild = matchComponent(component, critera);
		if (targetChild != null) {
			return this.resolve(targetChild.getName());
		}
		
		//if not match, find child
		for (Component subcomponent : component.getComponents()) {
			this.findChild(subcomponent, critera);
		}
		
		return null;
	}

	private Component matchComponent(Component component,
			Map<String, String> critera) {
		for (Entry<String, String> entry : critera.entrySet()) {
			if ("type".equalsIgnoreCase(entry.getKey())) {
				if (! component.getType().equals(entry.getValue())) {
					return null;
				}
			}
			if ("name".equalsIgnoreCase(entry.getKey())) {
				if (! component.getName().equals(entry.getValue())) {
					return null;
				}
			}
			if ("to".equalsIgnoreCase(entry.getKey())) {
				if (! component.getTo().equals(entry.getValue())) {
					return null;
				}
			}
		}
		
		return component;
	}

}
