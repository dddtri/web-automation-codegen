The goal of this project is to faciliate code implementation on page object & 
step interface to cucumber.  To use this tool, in nutshell, we provide json 
files that describes what the UI elements in interest look like, the tool then 
generates the page object & cucumber step classes for us.


```json
{"components" : [
    {
    	"title": "EPCS Gold Rx Signing",
      	"name" : "EPCSSigningPage",
      	"context": "epcs signing",
    	"type" : "page",
    	"components" : [
		
    		{"name": "device",
    		  "type": "dropdown",
    		 "locator": "id=selectedToken"
    		},
    		{"name": "passPhrase",
    		  "type": "input",
    		 "locator": "id=passphrasePwd"
    		},
    		{"name": "optPin",
    		  "type": "input",
    		 "locator": "id=otpPin"
    		},
    		{"name": "sendAndSign",
    		  "type": "button",
    		 "locator": "id=signNsendBtn"
    		}
    	  ]
     }
  ]
	  
}
```
